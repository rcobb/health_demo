## Monitoring

https://gitlab.com/sarahwaldner/waldner_demo/environments/836846/metrics

# Timeline of events

<!--

Here you should add the timeline of events, such as when the incident happened,
when action X was taken, etc.

-->

* YYYY-MM-DD XX:YY UTC: action X taken

## Communication


/assign @sarahwaldner  
/label ~SRE ~critical  
/due in 2 days  

